#!/bin/sh

set -ex
t=$(date)
tag=livenessprobe

logger -t $tag "$t"

sleep 2

res=$(tail -1 /var/log/$tag/messages.log | grep "$t" | wc -l)

if [ $res -eq 1 ]
then
    echo ok
else
    exit 1
fi