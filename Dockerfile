FROM alpine:edge

RUN  apk add --update rsyslog logrotate \
  && rm -rf /var/cache/apk/*

EXPOSE 514 514/udp

COPY ./etc/rsyslog.conf /etc/rsyslog.conf
COPY ./etc/logrotate.conf /etc/logrotate.conf
COPY ./livenessprobe.sh /livenessprobe.sh

RUN chmod 644 /etc/logrotate.conf \
    && chmod +x /livenessprobe.sh

CMD [ "rsyslogd", "-n" ]